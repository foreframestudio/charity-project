<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Charity_Project
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'charity-project' ) ); ?>"><?php printf( esc_html__( 'Proudly powered by %s', 'charity-project' ), 'WordPress' ); ?></a>
			<span class="sep"> | </span>
			<?php printf( esc_html__( 'Theme: %1$s by %2$s.', 'charity-project' ), 'charity-project', '<a href="https://foreframe.com/" rel="designer">Foreframe Studio</a>' ); ?>
		</div><!-- .site-info -->
        <!-- Save queries for analysis -->
        <?php
        if ( current_user_can( 'administrator' ) ) {
            global $wpdb;
            echo "<pre>";
            print_r( $wpdb->queries );
            echo "</pre>";
        }
        ?>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
